# How to download from youtube and tag

This will somewhat help with plex music

## Structure

```
artist/album/track_number - title.ext
```

## Important

Make sure to check "Prefer local metadata" in the plex library settings.

Otherwise this will not work.

## How

1. Install [yt-dlp](https://github.com/yt-dlp/yt-dlp)

2. Install ffmpeg if not avaliable

3. Create the artist and album folders as above  
    ```
    Various Authors/My Playlist Name/
    ```

4. Go into the album folder for the playlist

5. Use yt-dlp to download the album  
    ```sh
    yt-dlp -o "%(playlist_index)s - %(title)s.%(ext)s" -f 'ba' --download-archive 'archive.txt' -x --audio-format 'opus' <your playlist url>
    ```

    I used opus here but any yt-dlp supported audio format will work

    Depending on your needs, you might also be interested in the --playlist-reverse flag

6. Go back to the root folder

7. Copy in tag_from_name.py if not already exists

8. Install mutagen if not already installed  
    ```sh
    pip3 install mutagen
    ```

9. Run tag_from_name.py  

    ```sh
    python3 tag_from_name.py
    ```

