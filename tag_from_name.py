import mutagen
import os
import re

def tag_file(filepath, artist=None, album=None, track_num=None, title=None):
    file = mutagen.File(filepath)
    if artist:
        file['artist'] = artist
    if album:
        file['album'] = album
    if track_num:
        file['tracknumber'] = str(track_num)
    if title:
        file['title'] = title
    file.save()
    return file

def split_file_from_name(name):
    matches = re.match(r"^(\d+)\s-\s(.+)\.(.+)$", name)
    track_num, title, ext = matches[1], matches[2], matches[3]
    track_num = int(track_num)
    title = title.strip()
    ext = ext.strip()
    return track_num, title, ext

def tag_all():
    artists = os.scandir('./')
    for artist in artists:
        if not artist.is_dir():
            continue
        if artist.name == '.git':
            continue
        albums = os.scandir(artist.path)
        for album in albums:
            if not album.is_dir():
                continue
            tracks = os.scandir(album.path)
            for track in tracks:
                if not track.is_file():
                    continue
                track_num, title, ext = split_file_from_name(track.name)
                print(f"{artist.name} | {album.name} | {track_num} | {title} | {ext}")
                tag_file(track.path, artist=artist.name, album=album.name, track_num=track_num, title=title)



def main():
    tag_all()


if __name__ == '__main__':
    main()